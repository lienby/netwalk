import random
import re

import sys

import urllib
history = []
def loadPage(link, file_name):
    with open(file_name, "wb") as f:

            print "Loading %s" % link
            urllib.urlretrieve(link, file_name)
            
inp = raw_input("Starting URL: ")
loadPage(inp,"temp.html")
logfile = open("log.txt","w+")
logfile.write(inp+"\n")
history.append(inp)
nextbrowse = ""
while(1):
    htmltemp = open("temp.html","rb")
    data = htmltemp.read()
    htmltemp.close()
    try:
        urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', data)
        if len(urls) > 1:
            nextbrowse = random.choice(urls)
            history.append(nextbrowse)
            logfile.write(nextbrowse + "\n")
            loadPage(nextbrowse, "temp.html")
        else:
            print("Dead End. Returning To Previous Page.")
            nextbrowse = history[-2]
            loadPage(nextbrowse, "temp.html")
            history = []
            history.append(nextbrowse)

    except:
        print("Dead End. Returning To Previous Page.")
        nextbrowse = history[-2]
        loadPage(nextbrowse,"temp.html")
        history = []
        history.append(nextbrowse)
